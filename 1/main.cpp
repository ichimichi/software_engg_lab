#include <iostream>
#include "functions.h"

int main()
{
    int A[MAX];

    std::cout << std::endl
              << "Input ...." << std::endl;
    int n = getArrayData(A);

    printArrayData("Input : ", A, n);

    std::cout << std::endl
              << "Sorting ...." << std::endl;
    quickSort(A, 0, n - 1);

    printArrayData("After Quick Sort: ", A, n);

    std::cout << std::endl
              << "Searching ...." << std::endl;
    int element;
    std::cout << "Enter an element to search : ";
    std::cin >> element;
    int pos = binarySearch(A, 0, n - 1, element);
    if (pos == -1)
    {
        std::cout << element << " not found" << std::endl;
    }
    else
    {
        std::cout << element << " found at index " << pos << std::endl;
    }

    std::cout << std::endl
              << "GCD ...." << std::endl;
    int i1, i2;
    std::cout << "Enter any two indexes between (0-" << n - 1 << ") :";
    std::cin >> i1 >> i2;
    std::cout << "GCD(" << A[i1] << "," << A[i2] << ") = " << gcd(A[i1], A[i2]) << std::endl;

    return 0;
}