#define MAX 300
int getArrayData(int arr[]);
void printArrayData(std::string msg ,int arr[], int n);
void quickSort(int a[], int p, int r);
int binarySearch(int arr[], int l, int r, int x);
int gcd(int a, int b);