#include<iostream>

int getArrayData(int arr[]){
    int n;
    std::cout<<"Enter n : ";
    std::cin>>n;
    for (int i = 0; i < n; i++)
    {
        std::cout<<"Enter "<<i+1<<"th element : ";
        std::cin>>arr[i];
    }
    return n;
}

void printArrayData(std::string msg ,int arr[], int n){
    std::cout<<msg<<std::endl;
    std::cout<<"[ ";
    for( int i = 0; i < n ; i++){
        std::cout<<arr[i]<<" ";
    }
    std::cout<<" ]"<<std::endl;
}