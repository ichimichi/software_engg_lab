#include <iostream>
#include "ButtonFactory.cpp"

class Client
{
	ButtonFactory buttonFactoryObj;

public:
	void operation()
	{
		int type;
		std::cout << "Enter Type (0-2): ";
		std::cin >> type;
		buttonFactoryObj.getButton(type);
	}
};

int main()
{
	Client clientObj;
	clientObj.operation();
}