#include <iostream>
#include <string>

class Button
{
public:
  virtual Button *clone() = 0;
};

class CheckBox : public Button
{
  CheckBox(const CheckBox &c)
  {
    std::cout << "Check Box";
  }

public:
  CheckBox()
  {
  }

  Button *clone()
  {
    return new CheckBox(*this);
  }
};

class RadioButton : public Button
{
  RadioButton(const RadioButton &c)
  {
    std::cout << "Radio Button";
  }

public:
  RadioButton()
  {
  }

  Button *clone()
  {
    return new RadioButton(*this);
  }
};

class ToggleButton : public Button
{
  ToggleButton(const ToggleButton &c)
  {
    std::cout << "Toggle Button";
  }

public:
  ToggleButton()
  {
  }

  Button *clone()
  {
    return new ToggleButton(*this);
  }
};

class ButtonFactory
{

  Button *prototype[3] = {0};

public:
  void loadButtons()
  {
    prototype[0] = new RadioButton;
    prototype[1] = new CheckBox;
    prototype[2] = new ToggleButton;
  }

  ButtonFactory()
  {
    loadButtons();
  }

  Button *getButton(int type)
  {
    return prototype[type]->clone();
  }
};