#include <iostream>
class Singleton
{
    private:
    static int counter;
    static Singleton *singletonInstance;
    
    Singleton(){
        counter++;
        std::cout<<counter<<std::endl;
    }
    public:
    static Singleton* getInstance(){
        if(!singletonInstance){
            singletonInstance = new Singleton;
        }
        return singletonInstance;
    }
};

int Singleton::counter = 0 ;
Singleton* Singleton::singletonInstance = 0 ;

int main(){
    Singleton singleton1 = *Singleton::getInstance();
    
    Singleton::getInstance();
    Singleton singleton2 = *Singleton::getInstance();
    Singleton singleton3 = *Singleton::getInstance();


}