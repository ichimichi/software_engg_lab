#include <iostream>
class Weather
{
  private:
    double temp[24];
    double rain[24];
    static Weather *weatherInstance;

    Weather(){
        std::cout<<"!--initialize--!\n\n";
        randomize();
    }
    

  public:
    static Weather* getInstance(){
        if(!weatherInstance){
            weatherInstance = new Weather;
        }
        return weatherInstance;
    }
    
    double getTemp(int i)
    {
        return temp[i - 1];
    }

    double getRain(int i)
    {
        return rain[i - 1];
    }

    void setTemp(int i, double val)
    {
        temp[i - 1] = val;
    }
    void setRain(int i, double val)
    {
        rain[i - 1] = val;
    }

    void randomize()
    {
        std::srand(time(0));
        for (int i = 0; i < 24; i++)
        {
            temp[i] = 20 + std::rand() % (200 - 20 + 1);
            rain[i] = 20 + std::rand() % (200 - 20 + 1);

        }
    }

    void printData()
    {
        std::cout << "month\ttemp\train" << std::endl;
        for (int i = 0; i < 24; i++)
        {
            std::cout << i + 1 << "\t" << temp[i] << "\t" << rain[i] << std::endl;
        }
    }
};

Weather* Weather::weatherInstance = 0;
