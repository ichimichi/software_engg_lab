#include <iostream>
#include "Weather.cpp"
#include "FarmerHelp.cpp"
#include "TravelPlanner.cpp"

main(){
    Weather::getInstance()->printData();
    FarmerHelp farmerObj("rice");
    farmerObj.farmerSuggest();

    TravelPlanner travelObj("summer");
    travelObj.travelSuggest();

}