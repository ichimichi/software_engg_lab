#include <string>

class TravelPlanner
{
    int season;
    std::string seasonName;
    int suggestedMonth;
    Weather *weatherObj;

  public:
    TravelPlanner(std::string seasonName)
    {
        weatherObj = Weather::getInstance();
        TravelPlanner::seasonName = seasonName;
        if (seasonName == "summer")
            season = 1;
        if (seasonName == "winter")
            season = 2;
        if (seasonName == "rainy")
            season = 3;
    }

    int travelSuggest()
    {
        int avg;
        int max = 0, min = 999;
        switch (season)
        {
        case 1:
            for (int i = 0; i < 12; i++)
            {
                avg = weatherObj->getTemp(i) + weatherObj->getTemp(i + 12);
                if (avg > max)
                {
                    suggestedMonth = i + 1;
                    max = avg;
                }
            }

            std::cout << "Best Month to travel during " << seasonName << " is on " << getMonth(suggestedMonth) << std::endl;
            return suggestedMonth;
        case 2:
            for (int i = 0; i < 12; i++)
            {
                avg = weatherObj->getTemp(i) + weatherObj->getTemp(i + 12);
                if (avg < min)
                {
                    suggestedMonth = i + 1;
                    min = avg;
                }
            }

            std::cout << "Best Month to travel during " << seasonName << " is on " << getMonth(suggestedMonth) << std::endl;
            return suggestedMonth;
        case 3:
            for (int i = 0; i < 12; i++)
            {
                avg = weatherObj->getRain(i) + weatherObj->getRain(i + 12);
                if (avg > max)
                {
                    suggestedMonth = i + 1;
                    max = avg;
                }
            }

            std::cout << "Best Month to travel during " << seasonName << " is on " << getMonth(suggestedMonth) << std::endl;
            return suggestedMonth;
        }
    }

    std::string getMonth(int month)
    {
        month = (month) % 12;
        switch (month)
        {
        case 1:
            return "January";
        case 2:
            return "February";
        case 3:
            return "March";
        case 4:
            return "April";
        case 5:
            return "May";
        case 6:
            return "June";
        case 7:
            return "July";
        case 8:
            return "August";
        case 9:
            return "September";
        case 10:
            return "October";
        case 11:
            return "November";
        case 12:
            return "December";
        default:
            break;
        }
    }
};